OK, here is the apparmor profile for Chrome Version 104.0.5112.79 on Ubuntu 22.04 LTS Jammy Jellyfish. 2022-08-09. 

It started off from the version found at https://gist.github.com/mauron85/6b6d346b2fbd8dc9070711679546bfda which was coded by https://github.com/detrout/apparmor-det/. That code was from 2016 and doesn't work with the current Chrome and current Ubuntu.

This version's credit belong to https://hardenwindows11forsecurity.com .

It is not truely minimal, as I didn't list out all of the used libraries. But it doesn't allow /bin and /sbin executables (which are actually links to /usr/bin and /usr/sbin in Ubuntu). Chrome doesn't seem to use any. And it only allows specific reads to /dev, /sys, /run, and /etc folders, not blanket allow-all reads. 

To develop an apparmor profile you first do "strace /opt/google/chrome/chrome 2>output.txt" to run Chrome under strace, and browse to a few sites and stop and close it. The 2> will pipe the output to the file output.txt. Then search through the output file with gedit and CTRL-F search for "open" statements. This gives you all the files the program needs to use. There would be hundreds of these and you need patience. 

Note at the end of each line where it might say "file not found". You don't need to include those; because Chrome is made to work with several distro's and will try to open files at different directories specific to each distro until it finds what it needs. 

Also in each line that says 'open', it will say read only (O_RDONLY) or read-write (O_RDWR). You put r for read only and rw for read-write. A library file needs rights to read, so a 'r' would suffice; it doesn't need execute (ix) . A configuration file at, lets say, home/yourAccount/.config/google-chrome ... needs rw, because the program is writing configuration files to that directory, like a bookmark, or a Chrome setting. 

Usually, only one or two main program files in the program directory needs execute rights (ix).

Then you include some apparmor definitions from /etc/apparmor.d/abstractions and /etc/apparmor.d/tunables. They go like this: #include <abstractions/gnome>. You don't need a lot of these found in the abstraction libraries folder and tunables library folder. Remember, an apparmor profile should be minimalistic, or else you will allow the attacker to do a lot of things. An attacker has all the access that you write into your profile. So make sure you don't give away too many rights. 

Now you go through the list that you have made and eliminate the non-essential things. Not all features designed into a program are essential. For example Linux has dconf, sort of a Windows Registry thing to store settings and things. Most programs including Chrome already stores what it needs into a directory under your home account: home/yourAccount/.config/google-chrome ... . That is a tradition of Unix. So dconf is not necessary, at least in my view. Another one is DBUS; which is for inter-program communication. I don't need Chrome to talk to anything. Last thing I need is some attacker controlled program to send it a shutdown now message or a message to ask what site I am surfing to. System designers are stupid; they dream of this co-operative world where programs talk to each other and provide helpful features. "Oh you are surfing to Youtube, let me start this other helpful program for you". When in fact the user is only concentrating on the program at hand - Chrome, don't expect interuptions, and only an attacker is interested in asking Chrome to do this that and the other, apart from what the Chrome user is actively requesting on the program's own screen. 

Looking at my code I noticed that I specified 'network,' which allows every type of networking there is. And it includes the 'raw' type. And raw is not for videos either, there is a 'stream' type which should be for video streaming. So I now I list only the networking components needed.

Then I noticed I have previously specified 'ptrace', and capability 'sys_ptrace', which are required to use chrome_crashpad_handler.(if you look in chrome's folder you will find it)  I think that's because crashpad can backtrace chrome's code to find out which line caused the crash. Now that is only useful to Google. There is nothing that the crashpad can do to help me get out of a crash - you need to restart the program. So out the door ptrace goes. I don't need helpful debuggers pointing out where the code crashed to hackers. 

Then I noticed that ld.so.cache is readable. So if I ran a program yesterday which used such and such a library, it would be stored there in the cache and will speed up loading things when I start yesterday's program again. Well I only want what chrome actively needs. I don't want to accidentally supply a hacker with code libraries that he might need. So out that goes. 

Now on the top line of the file you wrote, write '/opt/google/chrome/chrome {' and at the last line put a closing brace }. 

Then you name the file opt.google.chrome.chrome  .

Copy the file to /etc/apparmor.d . Then run 'apparmor_parser -r /etc/apparmor.d/opt.google.chrome.chrome'. This activates the profile. Or if it fails it tells you of any errors in the file.

Now you may have missed elimnating non-essential features of Chrome. An apparmor profile is meant to allow necessary features. It should be made to be minimal, but sometimes we miss things. Go browse for firejail. Do 'apt install firejail.xxx.deb'. Then run 'firecfg'. which creates firejail profiles of your installed apps. This is a security program for Linux and is meant to sandbox a program so that it doesn't corrupt the system if you get hacked. By corrupt I mean for example a hacker installing a rootkit. It is based on Linux virtualization features, and give a protected program a virtualized view of the system. After a program closes, it wipes that environment away, and nothing it left but a pristine OS. Firejail installs a .desktop file into /home/yourAccount/.local/share/applications directory. And that should automatically show up in the Show Applications nine-dots screen. 

Firejail virtualization is necessary because the programs under it's protection, like Chrome, may have security vulnerabilities known only to hackers. If a vulnerability exists, then a hacker can send Chrome a specially crafted page and obtain rights to run their malware. Ordinarily you have to surf to that page on the inet, but that page could also be sent directly to you by the hacker. You say 'but how does the hacker know my ip address?'. If you are under attack, the hacker already knows a little about you, like what sites you like to surf to. Then they hack that site, and obtain your ip by what your web browser submits to the site. Not a really difficult thing to do, considering that many web sites don't take security precautions. (Thats why you see so many news about leaked credit card accounts. Only massive leaks get to the news, and small intrusions are not reported as per the law.) 

Firejail has a protection feature that changes your ip every time you start a protected program. What you do is edit the Chrome .desktop file in /home/yourAccount/.local/share/applications. CTRL-F to search for the lines that say "Exec". After the word 'firejail', and before the '/opt/google/chrome/chrome', you add these program options: 

 --ignore=private-dev --caps.drop=all --machine-id --net=enp1s0  --dns=9.9.9.9 --netfilter=/etc/firejail/nolocal.net  

This line is made for Ubuntu, and the --net=enp1s0 should be changed to --net=eth0 for other non Debian based distro's. 

The key thing to do in security is to have several 'layers' of security. If one layer fails, you should have another layer. Apparmor could only do so much. It is meant only to restrict what a program can use and do. Under strictly written profiles, it limits what an attacker could run. But if the attacker runs what is within those limits, then there's little that the profile can accomplish. That's why you need firejail. 


